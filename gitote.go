// +build go1.8

// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

// Gitote (Git + Rem(ote) = Gitote) - Software version control made simple.

/*

   _____ _ _        _
  / ____(_) |      | |
 | |  __ _| |_ ___ | |_ ___
 | | |_ | | __/ _ \| __/ _ \
 | |__| | | || (_) | ||  __/
  \_____|_|\__\___/ \__\___|

*/

package main // import "gitote/gitote"

import (
	"gitote/gitote/cmd"
	"gitote/gitote/pkg/setting"
	"os"

	raven "github.com/getsentry/raven-go"
	"github.com/urfave/cli"
)

// AppVer holds the current Gitote version
const AppVer = "1.0.2-prod-rc.20"

// APIVer holds the current Gitote API version
const APIVer = "v1"

func init() {
	raven.SetDSN("https://c711d9b100e74dfeb5fdee84a4d6c273:ff5ccef0870a48aea106d89b67a69710@sentry.io/1268720")
	setting.AppVer = AppVer
	setting.APIVer = APIVer
}

func main() {
	app := cli.NewApp()
	app.Name = "Gitote"
	app.Usage = "Software version control made simple"
	app.Version = AppVer
	app.Commands = []cli.Command{
		cmd.Web,
		cmd.Serv,
		cmd.Hook,
		cmd.Cert,
		cmd.Admin,
		cmd.Import,
	}
	app.Run(os.Args)
}
