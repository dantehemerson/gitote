Hello Everyone!

# Git + Rem(ote) = Gitote ❤️

#### Software version control made simple!

Welcome to the [gitote](https://gitote.in). We are so excited to have you. With your help, we can build out Gitote to be more stable and better serve our platform.

#### What is Gitote

Gitote is an **open source** end-to-end software development platform with built-in version control, issue tracking, code review, and more, it is an alternative to **GitHub**, **GitLab**, **Bitbucket** and many more!

#### Tech Stack

{% link https://dev.to/gitote/the-gitote--tech-stack-148a %}

**Platform:** https://gitote.in
**Source Code:** https://gitote.in/gitote/gitote

