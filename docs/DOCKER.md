# Docker for Gitote

## Usage

To keep your data out of Docker container, we do a volume (`/var/gitote` -> `/data`) here, and you can change it based on your situation.

```sh
# Pull image from Docker Hub.
$ docker pull gitote/gitote

# Create local directory for volume.
$ mkdir -p /var/gitote

# Use `docker run` for the first time.
$ docker run --name=gitote -p 10022:22 -p 10080:3000 -v /var/gitote:/data gitote/gitote

# Use `docker start` if you have stopped it.
$ docker start gitote
```

Note: It is important to map the Gitote ssh service from the container to the host and set the appropriate SSH Port and URI settings when setting up Gitote for the first time. To access and clone Gitote Git repositories with the above configuration you would use: `git clone ssh://git@hostname:10022/username/myrepo.git` for example.

Files will be store in local path `/var/gitote` in my case.

Directory `/var/gitote` keeps Git repositories and Gitote data:

    /var/gitote
    |-- git
    |   |-- gitote-repositories
    |-- ssh
    |   |-- # ssh public/private keys for Gitote
    |-- gitote
        |-- conf
        |-- data
        |-- log

#### Custom Directory

The "custom" directory may not be obvious in Docker environment. The `/var/gitote/gitote` (in the host) and `/data/gitote` (in the container) is already the "custom" directory and you do not need to create another layer but directly edit corresponding files under this directory.

### Volume With Data Container

If you're more comfortable with mounting data to a data container, the commands you execute at the first time will look like as follows:

```sh
# Create data container
docker run --name=gitote-data --entrypoint /bin/true gitote/gitote

# Use `docker run` for the first time.
docker run --name=gitote --volumes-from gitote-data -p 10022:22 -p 10080:3000 gitote/gitote
```

#### Using Docker 1.9 Volume Command

```sh
# Create docker volume.
$ docker volume create --name gitote-data

# Use `docker run` for the first time.
$ docker run --name=gitote -p 10022:22 -p 10080:3000 -v gitote-data:/data gitote/gitote
```

## Settings

### Application

Most of settings are obvious and easy to understand, but there are some settings can be confusing by running Gitote inside Docker:

- **Repository Root Path**: keep it as default value `/home/git/gitote-repositories` because `start.sh` already made a symbolic link for you.
- **Run User**: keep it as default value `git` because `build.sh` already setup a user with name `git`.
- **Domain**: fill in with Docker container IP (e.g. `192.168.99.100`). But if you want to access your Gitote instance from a different physical machine, please fill in with the hostname or IP address of the Docker host machine.
- **SSH Port**: Use the exposed port from Docker container. For example, your SSH server listens on `22` inside Docker, **but** you expose it by `10022:22`, then use `10022` for this value. **Builtin SSH server is not recommended inside Docker Container**
- **HTTP Port**: Use port you want Gitote to listen on inside Docker container. For example, your Gitote listens on `3000` inside Docker, **and** you expose it by `10080:3000`, but you still use `3000` for this value.
- **Application URL**: Use combination of **Domain** and **exposed HTTP Port** values (e.g. `http://192.168.99.100:10080/`).

### Container Options

This container have some options available via environment variables, these options are opt-in features that can help the administration of this container:

- **SOCAT_LINK**:
  - <u>Possible value:</u>
      `true`, `false`, `1`, `0`
  - <u>Default:</u>
      `true`
  - <u>Action:</u>
      Bind linked docker container to localhost socket using socat.
      Any exported port from a linked container will be binded to the matching port on localhost.
  - <u>Disclaimer:</u>
      As this option rely on the environment variable created by docker when a container is linked, this option should be deactivated in managed environment such as Rancher or Kubernetes (set to `0` or `false`)
- **RUN_CROND**:
  - <u>Possible value:</u>
      `true`, `false`, `1`, `0`
  - <u>Default:</u>
      `false`
  - <u>Action:</u>
      Request crond to be run inside the container. Its default configuration will periodically run all scripts from `/etc/periodic/${period}` but custom crontabs can be added to `/var/spool/cron/crontabs/`.

## Upgrade

❗❗❗ **Make sure you have volumed data to somewhere outside Docker container** ❗❗❗

Steps to upgrade Gitote with Docker:

- `docker pull gitote/gitote`
- `docker stop gitote`
- `docker rm gitote`
- Finally, create a container for the first time and don't forget to do the same for the volume and port mapping.

## Known Issues

- The docker container cannot currently be built on Raspberry 1 (armv6l) as our base image `alpine` does not have a `go` package available for this platform.
