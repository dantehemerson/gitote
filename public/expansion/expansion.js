var nextWord = (function() {
    var wordArray = [
        'Gitote Is Tasty',
        'Get In Touch',
        'Get Inside Test',
        'Get It Tested',
        'GNU Interactive Tools',
        'Goods In Transit',
        'Goods In Transport',
        'Geek In Training',
        'Goddess In Training',
        'Gentleman In Training',
        'Geologist In Training',
        'Ghost In Training',
        'Geeks In Town',
        'General Information Test',
        'Group Inclusive Tour',
        'Geometric Invariant Theory',
        'Graphic Information Technology',
        'Graphic Image Technology',
        'Great Innovative Technology',
        'Global Information Tracker'
    ];
    var copy;
    return function() {
        if (!copy || !copy.length) copy = wordArray.slice();
        return copy.splice(Math.random() * copy.length | 0, 1);
    }
}());
