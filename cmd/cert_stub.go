// +build !cert

// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package cmd

import (
	"fmt"
	"os"

	"github.com/urfave/cli"
)

// Cert Initialize cert CLI
var Cert = cli.Command{
	Name:        "cert",
	Usage:       "Generate self-signed certificate",
	Description: `Please use build tags "cert" to rebuild Gitote in order to have this ability`,
	Action:      runCert,
}

// runCert builds cert CLI
func runCert(ctx *cli.Context) error {
	fmt.Println("Command cert not available, please use build tags 'cert' to rebuild.")
	os.Exit(1)

	return nil
}
