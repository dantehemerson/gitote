// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package context

import (
	"gopkg.in/macaron.v1"

	"gitote/gitote/models"
	"gitote/gitote/models/errors"
)

// ParamsUser is the wrapper type of the target user defined by URL parameter, namely ':username'.
type ParamsUser struct {
	*models.User
}

// InjectParamsUser returns a handler that retrieves target user based on URL parameter ':username',
// and injects it as *ParamsUser.
func InjectParamsUser() macaron.Handler {
	return func(c *Context) {
		user, err := models.GetUserByName(c.Params(":username"))
		if err != nil {
			c.NotFoundOrServerError("GetUserByName", errors.IsUserNotExist, err)
			return
		}
		c.Map(&ParamsUser{user})
	}
}
