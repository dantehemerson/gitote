// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package context

import (
	"gitote/gitote/models"
)

// APIOrganization contains organization and team
type APIOrganization struct {
	Organization *models.User
	Team         *models.Team
}
