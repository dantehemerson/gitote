First of all, read https://www.chiark.greenend.org.uk/~sgtatham/bugs.html.

If you don't want to read, it's up to you. But don't waste your time continue reporting.

The issue will be closed without any reasons if it does not satisfy any of following requirements:

1. Please speak English
2. Please do not end your title with a question mark or period.
3. Please take a moment to search that an issue doesn't already exist.
4. Please give all relevant information below for bug reports; incomplete details considered invalid report.

**You MUST delete above content including this line before posting; too lazy to take this action considered invalid report.**

- Gitote version (or commit ref): 
- Git version: 
- Operating system: 
- Database (use `[x]`):
  - [ ] PostgreSQL
  - [ ] MySQL
  - [ ] MSSQL
  - [ ] SQLite
- Can you reproduce the bug at https://gitote.in:
  - [ ] Yes: provide example URL
  - [ ] No: explain why
- Log gist (usually found in `log/gitote.log`):

## Description

