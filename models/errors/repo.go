// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package errors

import "fmt"

// RepoNotExist represents a "RepoNotExist" kind of error
type RepoNotExist struct {
	ID     int64
	UserID int64
	Name   string
}

// IsRepoNotExist checks if an error is a RepoNotExist
func IsRepoNotExist(err error) bool {
	_, ok := err.(RepoNotExist)
	return ok
}

func (err RepoNotExist) Error() string {
	return fmt.Sprintf("repository does not exist [id: %d, user_id: %d, name: %s]", err.ID, err.UserID, err.Name)
}

// ReachLimitOfRepo represents a "ReachLimitOfRepo" kind of error
type ReachLimitOfRepo struct {
	Limit int
}

// IsReachLimitOfRepo checks if an error is a ReachLimitOfRepo
func IsReachLimitOfRepo(err error) bool {
	_, ok := err.(ReachLimitOfRepo)
	return ok
}

func (err ReachLimitOfRepo) Error() string {
	return fmt.Sprintf("user has reached maximum limit of repositories [limit: %d]", err.Limit)
}

// InvalidRepoReference represents a "InvalidRepoReference" kind of error
type InvalidRepoReference struct {
	Ref string
}

// IsInvalidRepoReference checks if an error is a InvalidRepoReference
func IsInvalidRepoReference(err error) bool {
	_, ok := err.(InvalidRepoReference)
	return ok
}

func (err InvalidRepoReference) Error() string {
	return fmt.Sprintf("invalid repository reference [ref: %s]", err.Ref)
}

// MirrorNotExist represents a "MirrorNotExist" kind of error
type MirrorNotExist struct {
	RepoID int64
}

// IsMirrorNotExist checks if an error is a MirrorNotExist
func IsMirrorNotExist(err error) bool {
	_, ok := err.(MirrorNotExist)
	return ok
}

func (err MirrorNotExist) Error() string {
	return fmt.Sprintf("mirror does not exist [repo_id: %d]", err.RepoID)
}

// BranchAlreadyExists represents a "BranchAlreadyExists" kind of error
type BranchAlreadyExists struct {
	Name string
}

// IsBranchAlreadyExists checks if an error is a BranchAlreadyExists
func IsBranchAlreadyExists(err error) bool {
	_, ok := err.(BranchAlreadyExists)
	return ok
}

func (err BranchAlreadyExists) Error() string {
	return fmt.Sprintf("branch already exists [name: %s]", err.Name)
}

// ErrBranchNotExist represents a "ErrBranchNotExist" kind of error
type ErrBranchNotExist struct {
	Name string
}

// IsErrBranchNotExist checks if an error is a ErrBranchNotExist
func IsErrBranchNotExist(err error) bool {
	_, ok := err.(ErrBranchNotExist)
	return ok
}

func (err ErrBranchNotExist) Error() string {
	return fmt.Sprintf("branch does not exist [name: %s]", err.Name)
}
