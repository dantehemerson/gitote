// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package errors

import "fmt"

// TwoFactorNotFound represents a "TwoFactorNotFound" kind of error
type TwoFactorNotFound struct {
	UserID int64
}

// IsTwoFactorNotFound checks if an error is a TwoFactorNotFound
func IsTwoFactorNotFound(err error) bool {
	_, ok := err.(TwoFactorNotFound)
	return ok
}

func (err TwoFactorNotFound) Error() string {
	return fmt.Sprintf("two-factor authentication does not found [user_id: %d]", err.UserID)
}

// TwoFactorRecoveryCodeNotFound represents a "TwoFactorRecoveryCodeNotFound" kind of error
type TwoFactorRecoveryCodeNotFound struct {
	Code string
}

// IsTwoFactorRecoveryCodeNotFound checks if an error is a TwoFactorRecoveryCodeNotFound
func IsTwoFactorRecoveryCodeNotFound(err error) bool {
	_, ok := err.(TwoFactorRecoveryCodeNotFound)
	return ok
}

func (err TwoFactorRecoveryCodeNotFound) Error() string {
	return fmt.Sprintf("two-factor recovery code does not found [code: %s]", err.Code)
}
