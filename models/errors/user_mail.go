// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package errors

import "fmt"

// EmailNotFound represents a "EmailNotFound" kind of error
type EmailNotFound struct {
	Email string
}

// IsEmailNotFound checks if an error is a EmailNotFound
func IsEmailNotFound(err error) bool {
	_, ok := err.(EmailNotFound)
	return ok
}

func (err EmailNotFound) Error() string {
	return fmt.Sprintf("email is not found [email: %s]", err.Email)
}

// EmailNotVerified represents a "EmailNotVerified" kind of error
type EmailNotVerified struct {
	Email string
}

// IsEmailNotVerified checks if an error is a EmailNotVerified
func IsEmailNotVerified(err error) bool {
	_, ok := err.(EmailNotVerified)
	return ok
}

func (err EmailNotVerified) Error() string {
	return fmt.Sprintf("email has not been verified [email: %s]", err.Email)
}
