// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package errors

import "fmt"

// EmptyName represents a "RepoNotExist" kind of error
type EmptyName struct{}

// IsEmptyName checks if an error is a EmptyName
func IsEmptyName(err error) bool {
	_, ok := err.(EmptyName)
	return ok
}

func (err EmptyName) Error() string {
	return "empty name"
}

// UserNotExist represents a "UserNotExist" kind of error
type UserNotExist struct {
	UserID int64
	Name   string
}

// IsUserNotExist checks if an error is a UserNotExist
func IsUserNotExist(err error) bool {
	_, ok := err.(UserNotExist)
	return ok
}

func (err UserNotExist) Error() string {
	return fmt.Sprintf("user does not exist [user_id: %d, name: %s]", err.UserID, err.Name)
}

// UserNotKeyOwner represents a "UserNotKeyOwner" kind of error
type UserNotKeyOwner struct {
	KeyID int64
}

// IsUserNotKeyOwner checks if an error is a UserNotKeyOwner
func IsUserNotKeyOwner(err error) bool {
	_, ok := err.(UserNotKeyOwner)
	return ok
}

func (err UserNotKeyOwner) Error() string {
	return fmt.Sprintf("user is not the owner of public key [key_id: %d]", err.KeyID)
}
