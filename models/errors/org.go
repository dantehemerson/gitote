// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package errors

import "fmt"

// TeamNotExist represents a "TeamNotExist" kind of error
type TeamNotExist struct {
	TeamID int64
	Name   string
}

// IsTeamNotExist checks if an error is a TeamNotExist
func IsTeamNotExist(err error) bool {
	_, ok := err.(TeamNotExist)
	return ok
}

func (err TeamNotExist) Error() string {
	return fmt.Sprintf("team does not exist [team_id: %d, name: %s]", err.TeamID, err.Name)
}
