// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package errors

import "fmt"

// IssueNotExist represents issue not exist
type IssueNotExist struct {
	ID     int64
	RepoID int64
	Index  int64
}

// IsIssueNotExist returns true if issue not exist
func IsIssueNotExist(err error) bool {
	_, ok := err.(IssueNotExist)
	return ok
}

func (err IssueNotExist) Error() string {
	return fmt.Sprintf("issue does not exist [id: %d, repo_id: %d, index: %d]", err.ID, err.RepoID, err.Index)
}

// InvalidIssueReference represents invalid issue reference
type InvalidIssueReference struct {
	Ref string
}

// IsInvalidIssueReference returns true if invalid issue reference
func IsInvalidIssueReference(err error) bool {
	_, ok := err.(InvalidIssueReference)
	return ok
}

func (err InvalidIssueReference) Error() string {
	return fmt.Sprintf("invalid issue reference [ref: %s]", err.Ref)
}
