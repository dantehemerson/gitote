// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package errors

import "fmt"

// LoginSourceNotExist represents a "LoginSourceNotExist" kind of error
type LoginSourceNotExist struct {
	ID int64
}

// IsLoginSourceNotExist checks if an error is a LoginSourceNotExist
func IsLoginSourceNotExist(err error) bool {
	_, ok := err.(LoginSourceNotExist)
	return ok
}

func (err LoginSourceNotExist) Error() string {
	return fmt.Sprintf("login source does not exist [id: %d]", err.ID)
}

// LoginSourceNotActivated represents a "LoginSourceNotActivated" kind of error
type LoginSourceNotActivated struct {
	SourceID int64
}

// IsLoginSourceNotActivated checks if an error is a LoginSourceNotActivated
func IsLoginSourceNotActivated(err error) bool {
	_, ok := err.(LoginSourceNotActivated)
	return ok
}

func (err LoginSourceNotActivated) Error() string {
	return fmt.Sprintf("login source is not activated [source_id: %d]", err.SourceID)
}

// InvalidLoginSourceType represents a "InvalidLoginSourceType" kind of error
type InvalidLoginSourceType struct {
	Type interface{}
}

// IsInvalidLoginSourceType checks if an error is a InvalidLoginSourceType
func IsInvalidLoginSourceType(err error) bool {
	_, ok := err.(InvalidLoginSourceType)
	return ok
}

func (err InvalidLoginSourceType) Error() string {
	return fmt.Sprintf("invalid login source type [type: %v]", err.Type)
}

// LoginSourceMismatch represents a "LoginSourceMismatch" kind of error
type LoginSourceMismatch struct {
	Expect int64
	Actual int64
}

// IsLoginSourceMismatch checks if an error is a LoginSourceMismatch
func IsLoginSourceMismatch(err error) bool {
	_, ok := err.(LoginSourceMismatch)
	return ok
}

func (err LoginSourceMismatch) Error() string {
	return fmt.Sprintf("login source mismatch [expect: %d, actual: %d]", err.Expect, err.Actual)
}
