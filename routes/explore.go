// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package routes

import (
	"gitote/gitote/models"
	"gitote/gitote/pkg/context"
	"gitote/gitote/pkg/setting"

	"gitlab.com/yoginth/paginater"
)

const (
	// ExploreTPL page template
	ExploreTPL = "explore/home"

	// ExploreReposTPL page template
	ExploreReposTPL = "explore/repos"

	// ExploreUsersTPL page template
	ExploreUsersTPL = "explore/users"

	// ExploreTrendingTPL page template
	ExploreTrendingTPL = "explore/trending"

	// ExploreOrganizationsTPL page template
	ExploreOrganizationsTPL = "explore/organizations"
)

// Explore shows explore page
func Explore(c *context.Context) {
	c.Data["Title"] = c.Tr("explore")
	c.Data["PageIsExplore"] = true
	c.Data["PageIsExploreRepositories"] = true

	repos, count, err := models.SearchRepositoryByName(&models.SearchRepoOptions{
		UserID:   c.UserID(),
		OrderBy:  "num_stars DESC",
		PageSize: 4,
	})
	if err != nil {
		c.ServerError("SearchRepositoryByName", err)
		return
	}
	c.Data["Total"] = count

	if err = models.RepositoryList(repos).LoadAttributes(); err != nil {
		c.ServerError("RepositoryList.LoadAttributes", err)
		return
	}
	c.Data["Repos"] = repos

	c.Success(ExploreTPL)
}

// ExploreRepos shows explore repositories page
func ExploreRepos(c *context.Context) {
	c.Data["Title"] = c.Tr("explore")
	c.Data["PageIsExplore"] = true
	c.Data["PageIsExploreRepositories"] = true

	page := c.QueryInt("page")
	if page <= 0 {
		page = 1
	}

	keyword := c.Query("q")
	// Search a repository
	repos, count, err := models.SearchRepositoryByName(&models.SearchRepoOptions{
		Keyword:  keyword,
		UserID:   c.UserID(),
		OrderBy:  "updated_unix DESC",
		Page:     page,
		PageSize: setting.UI.ExplorePagingNum,
	})
	if err != nil {
		c.ServerError("SearchRepositoryByName", err)
		return
	}
	c.Data["Keyword"] = keyword
	c.Data["Total"] = count
	c.Data["Page"] = paginater.New(int(count), setting.UI.ExplorePagingNum, page, 5)

	if err = models.RepositoryList(repos).LoadAttributes(); err != nil {
		c.ServerError("RepositoryList.LoadAttributes", err)
		return
	}
	c.Data["Repos"] = repos

	c.Success(ExploreReposTPL)
}

// ExploreTrending shows trending repositories page
func ExploreTrending(c *context.Context) {
	c.Data["Title"] = c.Tr("explore.trending")
	c.Data["PageIsTrending"] = true

	repos, count, err := models.SearchRepositoryByName(&models.SearchRepoOptions{
		UserID:   c.UserID(),
		OrderBy:  "num_stars DESC",
		PageSize: 15,
	})
	if err != nil {
		c.ServerError("SearchRepositoryByName", err)
		return
	}
	c.Data["Total"] = count

	if err = models.RepositoryList(repos).LoadAttributes(); err != nil {
		c.ServerError("RepositoryList.LoadAttributes", err)
		return
	}
	c.Data["Repos"] = repos

	c.Success(ExploreTrendingTPL)
}

// UserSearchOptions is the structure of the options choosed by the user
type UserSearchOptions struct {
	Type     models.UserType
	Counter  func() int64
	Ranger   func(int, int) ([]*models.User, error)
	PageSize int
	OrderBy  string
	TplName  string
}

// RenderUserSearch renders user search
func RenderUserSearch(c *context.Context, opts *UserSearchOptions) {
	page := c.QueryInt("page")
	if page <= 1 {
		page = 1
	}

	var (
		users []*models.User
		count int64
		err   error
	)

	keyword := c.Query("q")
	if len(keyword) == 0 {
		users, err = opts.Ranger(page, opts.PageSize)
		if err != nil {
			c.ServerError("Ranger", err)
			return
		}
		count = opts.Counter()
	} else {
		users, count, err = models.SearchUserByName(&models.SearchUserOptions{
			Keyword:  keyword,
			Type:     opts.Type,
			OrderBy:  opts.OrderBy,
			Page:     page,
			PageSize: opts.PageSize,
		})
		if err != nil {
			c.ServerError("SearchUserByName", err)
			return
		}
	}
	c.Data["Keyword"] = keyword
	c.Data["Total"] = count
	c.Data["Page"] = paginater.New(int(count), opts.PageSize, page, 5)
	c.Data["Users"] = users

	c.Success(opts.TplName)
}

// ExploreUsers shows explore users page
func ExploreUsers(c *context.Context) {
	c.Data["Title"] = c.Tr("explore")
	c.Data["PageIsExplore"] = true
	c.Data["PageIsExploreUsers"] = true

	RenderUserSearch(c, &UserSearchOptions{
		Type:     models.UserTypeIndividual,
		Counter:  models.CountUsers,
		Ranger:   models.Users,
		PageSize: setting.UI.ExplorePagingNum,
		OrderBy:  "updated_unix DESC",
		TplName:  ExploreUsersTPL,
	})
}

//ExploreOrganizations shows explore organizations page
func ExploreOrganizations(c *context.Context) {
	c.Data["Title"] = c.Tr("explore")
	c.Data["PageIsExplore"] = true
	c.Data["PageIsExploreOrganizations"] = true

	RenderUserSearch(c, &UserSearchOptions{
		Type:     models.UserTypeOrganization,
		Counter:  models.CountOrganizations,
		Ranger:   models.Organizations,
		PageSize: setting.UI.ExplorePagingNum,
		OrderBy:  "updated_unix DESC",
		TplName:  ExploreOrganizationsTPL,
	})
}
