// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package org

import (
	"gitote/gitote/models"
	"gitote/gitote/models/errors"
	"gitote/gitote/pkg/context"
	"gitote/gitote/pkg/form"
	"gitote/gitote/pkg/setting"
	"gitote/gitote/routes/user"
	"strings"

	log "gopkg.in/clog.v1"
)

const (
	// SettingsOptionsTPL page template
	SettingsOptionsTPL = "org/settings/options"

	// SettingsDeleteTPL page template
	SettingsDeleteTPL = "org/settings/delete"

	// SettingsWebhooksTPL page template
	SettingsWebhooksTPL = "org/settings/webhooks"
)

// Settings shows the organization settings page
func Settings(c *context.Context) {
	c.Data["Title"] = c.Tr("org.settings")
	c.Data["PageIsSettingsOptions"] = true
	c.HTML(200, SettingsOptionsTPL)
}

// SettingsPost updates organization settings with the body fields
func SettingsPost(c *context.Context, f form.UpdateOrgSetting) {
	c.Data["Title"] = c.Tr("org.settings")
	c.Data["PageIsSettingsOptions"] = true

	if c.HasError() {
		c.HTML(200, SettingsOptionsTPL)
		return
	}

	org := c.Org.Organization

	// Check if organization name has been changed.
	if org.LowerName != strings.ToLower(f.Name) {
		isExist, err := models.IsUserExist(org.ID, f.Name)
		if err != nil {
			c.Handle(500, "IsUserExist", err)
			return
		} else if isExist {
			c.Data["OrgName"] = true
			c.RenderWithErr(c.Tr("form.username_been_taken"), SettingsOptionsTPL, &f)
			return
		} else if err = models.ChangeUserName(org, f.Name); err != nil {
			c.Data["OrgName"] = true
			switch {
			case models.IsErrNameReserved(err):
				c.RenderWithErr(c.Tr("user.form.name_reserved"), SettingsOptionsTPL, &f)
			case models.IsErrNamePatternNotAllowed(err):
				c.RenderWithErr(c.Tr("user.form.name_pattern_not_allowed"), SettingsOptionsTPL, &f)
			default:
				c.Handle(500, "ChangeUserName", err)
			}
			return
		}
		// reset c.org.OrgLink with new name
		c.Org.OrgLink = setting.AppSubURL + "/org/" + f.Name
		log.Trace("Organization name changed: %s -> %s", org.Name, f.Name)
	}
	// In case it's just a case change.
	org.Name = f.Name
	org.LowerName = strings.ToLower(f.Name)

	if c.User.IsAdmin {
		org.MaxRepoCreation = f.MaxRepoCreation
	}

	org.FullName = f.FullName
	org.Description = f.Description
	org.Website = f.Website
	org.Location = f.Location
	org.IsVerified = f.IsVerified

	// Update organization
	if err := models.UpdateUser(org); err != nil {
		c.Handle(500, "UpdateUser", err)
		return
	}
	log.Trace("Organization setting updated: %s", org.Name)
	c.Flash.Success(c.Tr("org.settings.update_setting_success"))
	c.Redirect(c.Org.OrgLink + "/settings")
}

// SettingsAvatar updates organization avatar
func SettingsAvatar(c *context.Context, f form.Avatar) {
	f.Source = form.AvatarLocal

	// Update avatar
	if err := user.UpdateAvatarSetting(c, f, c.Org.Organization); err != nil {
		c.Flash.Error(err.Error())
	} else {
		c.Flash.Success(c.Tr("org.settings.update_avatar_success"))
	}

	// Redirect to "/settings"
	c.Redirect(c.Org.OrgLink + "/settings")
}

// SettingsDeleteAvatar deletes organization avatar
func SettingsDeleteAvatar(c *context.Context) {
	if err := c.Org.Organization.DeleteAvatar(); err != nil {
		c.Flash.Error(err.Error())
	}

	c.Redirect(c.Org.OrgLink + "/settings")
}

// SettingsDelete deletes an organization
func SettingsDelete(c *context.Context) {
	c.Title("org.settings")
	c.PageIs("SettingsDelete")

	org := c.Org.Organization
	if c.Req.Method == "POST" {
		// Check if user is logged in
		if _, err := models.UserLogin(c.User.Name, c.Query("password"), c.User.LoginSource); err != nil {
			if errors.IsUserNotExist(err) {
				c.RenderWithErr(c.Tr("form.enterred_invalid_password"), SettingsDeleteTPL, nil)
			} else {
				c.ServerError("UserLogin", err)
			}
			return
		}

		// Delete organization
		if err := models.DeleteOrganization(org); err != nil {
			if models.IsErrUserOwnRepos(err) {
				c.Flash.Error(c.Tr("form.org_still_own_repo"))
				c.Redirect(c.Org.OrgLink + "/settings/delete")
			} else {
				c.ServerError("DeleteOrganization", err)
			}
		} else {
			log.Trace("Organization deleted: %s", org.Name)
			c.Redirect(setting.AppSubURL + "/")
		}
		return
	}

	c.Success(SettingsDeleteTPL)
}

// Webhooks shows organization webhooks settings
func Webhooks(c *context.Context) {
	c.Data["Title"] = c.Tr("org.settings")
	c.Data["PageIsSettingsHooks"] = true
	c.Data["BaseLink"] = c.Org.OrgLink
	c.Data["Description"] = c.Tr("org.settings.hooks_desc")
	c.Data["Types"] = setting.Webhook.Types

	ws, err := models.GetWebhooksByOrgID(c.Org.Organization.ID)
	if err != nil {
		c.Handle(500, "GetWebhooksByOrgId", err)
		return
	}

	c.Data["Webhooks"] = ws
	c.HTML(200, SettingsWebhooksTPL)
}

// DeleteWebhook deletes an organization webhook
func DeleteWebhook(c *context.Context) {

	// Delete the webhook
	if err := models.DeleteWebhookOfOrgByID(c.Org.Organization.ID, c.QueryInt64("id")); err != nil {
		c.Flash.Error("DeleteWebhookByOrgID: " + err.Error())
	} else {
		c.Flash.Success(c.Tr("repo.settings.webhook_deletion_success"))
	}

	c.JSON(200, map[string]interface{}{
		"redirect": c.Org.OrgLink + "/settings/hooks",
	})
}
