// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package misc

import (
	"gitote/gitote/pkg/context"
	"gitote/gitote/pkg/markup"

	api "gitlab.com/gitote/go-gitote-client"
)

// Markdown render markdown document to HTML
func Markdown(c *context.APIContext, form api.MarkdownOption) {
	if c.HasAPIError() {
		c.Error(422, "", c.GetErrMsg())
		return
	}

	if len(form.Text) == 0 {
		c.Write([]byte(""))
		return
	}

	switch form.Mode {
	case "gfm":
		c.Write(markup.Markdown([]byte(form.Text), form.Context, nil))
	default:
		c.Write(markup.RawMarkdown([]byte(form.Text), ""))
	}
}

// MarkdownRaw render raw markdown HTML
func MarkdownRaw(c *context.APIContext) {
	body, err := c.Req.Body().Bytes()
	if err != nil {
		c.Error(422, "", err)
		return
	}
	c.Write(markup.RawMarkdown(body, ""))
}
