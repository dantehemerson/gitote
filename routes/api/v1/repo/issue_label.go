// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package repo

import (
	"gitote/gitote/models"
	"gitote/gitote/models/errors"
	"gitote/gitote/pkg/context"

	api "gitlab.com/gitote/go-gitote-client"
)

// ListIssueLabels list all the labels of an issue
func ListIssueLabels(c *context.APIContext) {
	issue, err := models.GetIssueByIndex(c.Repo.Repository.ID, c.ParamsInt64(":index"))
	if err != nil {
		if errors.IsIssueNotExist(err) {
			c.Status(404)
		} else {
			c.Error(500, "GetIssueByIndex", err)
		}
		return
	}

	apiLabels := make([]*api.Label, len(issue.Labels))
	for i := range issue.Labels {
		apiLabels[i] = issue.Labels[i].APIFormat()
	}
	c.JSON(200, &apiLabels)
}

// AddIssueLabels add labels for an issue
func AddIssueLabels(c *context.APIContext, form api.IssueLabelsOption) {
	if !c.Repo.IsWriter() {
		c.Status(403)
		return
	}

	issue, err := models.GetIssueByIndex(c.Repo.Repository.ID, c.ParamsInt64(":index"))
	if err != nil {
		if errors.IsIssueNotExist(err) {
			c.Status(404)
		} else {
			c.Error(500, "GetIssueByIndex", err)
		}
		return
	}

	labels, err := models.GetLabelsInRepoByIDs(c.Repo.Repository.ID, form.Labels)
	if err != nil {
		c.Error(500, "GetLabelsInRepoByIDs", err)
		return
	}

	if err = issue.AddLabels(c.User, labels); err != nil {
		c.Error(500, "AddLabels", err)
		return
	}

	labels, err = models.GetLabelsByIssueID(issue.ID)
	if err != nil {
		c.Error(500, "GetLabelsByIssueID", err)
		return
	}

	apiLabels := make([]*api.Label, len(labels))
	for i := range labels {
		apiLabels[i] = issue.Labels[i].APIFormat()
	}
	c.JSON(200, &apiLabels)
}

// DeleteIssueLabel delete a label for an issue
func DeleteIssueLabel(c *context.APIContext) {
	if !c.Repo.IsWriter() {
		c.Status(403)
		return
	}

	issue, err := models.GetIssueByIndex(c.Repo.Repository.ID, c.ParamsInt64(":index"))
	if err != nil {
		if errors.IsIssueNotExist(err) {
			c.Status(404)
		} else {
			c.Error(500, "GetIssueByIndex", err)
		}
		return
	}

	label, err := models.GetLabelOfRepoByID(c.Repo.Repository.ID, c.ParamsInt64(":id"))
	if err != nil {
		if models.IsErrLabelNotExist(err) {
			c.Error(422, "", err)
		} else {
			c.Error(500, "GetLabelInRepoByID", err)
		}
		return
	}

	if err := models.DeleteIssueLabel(issue, label); err != nil {
		c.Error(500, "DeleteIssueLabel", err)
		return
	}

	c.Status(204)
}

// ReplaceIssueLabels replace labels for an issue
func ReplaceIssueLabels(c *context.APIContext, form api.IssueLabelsOption) {
	if !c.Repo.IsWriter() {
		c.Status(403)
		return
	}

	issue, err := models.GetIssueByIndex(c.Repo.Repository.ID, c.ParamsInt64(":index"))
	if err != nil {
		if errors.IsIssueNotExist(err) {
			c.Status(404)
		} else {
			c.Error(500, "GetIssueByIndex", err)
		}
		return
	}

	labels, err := models.GetLabelsInRepoByIDs(c.Repo.Repository.ID, form.Labels)
	if err != nil {
		c.Error(500, "GetLabelsInRepoByIDs", err)
		return
	}

	if err := issue.ReplaceLabels(labels); err != nil {
		c.Error(500, "ReplaceLabels", err)
		return
	}

	labels, err = models.GetLabelsByIssueID(issue.ID)
	if err != nil {
		c.Error(500, "GetLabelsByIssueID", err)
		return
	}

	apiLabels := make([]*api.Label, len(labels))
	for i := range labels {
		apiLabels[i] = issue.Labels[i].APIFormat()
	}
	c.JSON(200, &apiLabels)
}

// ClearIssueLabels delete all the labels for an issue
func ClearIssueLabels(c *context.APIContext) {
	if !c.Repo.IsWriter() {
		c.Status(403)
		return
	}

	issue, err := models.GetIssueByIndex(c.Repo.Repository.ID, c.ParamsInt64(":index"))
	if err != nil {
		if errors.IsIssueNotExist(err) {
			c.Status(404)
		} else {
			c.Error(500, "GetIssueByIndex", err)
		}
		return
	}

	if err := issue.ClearLabels(c.User); err != nil {
		c.Error(500, "ClearLabels", err)
		return
	}

	c.Status(204)
}
