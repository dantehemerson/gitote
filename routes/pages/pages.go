// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package pages

import (
	"gitote/gitote/pkg/context"
)

const (
	// AboutTPL page template
	AboutTPL = "pages/about"

	// FaqTPL page template
	FaqTPL = "pages/faq"

	// PrivacyTPL page template
	PrivacyTPL = "pages/privacy"

	// TosTPL page template
	TosTPL = "pages/tos"

	// BrandTPL page template
	BrandTPL = "pages/brand"

	// ContactTPL page template
	ContactTPL = "pages/contact"

	// ContributeTPL page template
	ContributeTPL = "pages/contribute"

	// SecurityTPL page template
	SecurityTPL = "pages/security"

	// VerifiedTPL page template
	VerifiedTPL = "pages/verified"

	// MakersTPL page template
	MakersTPL = "pages/makers"

	// HelpTPL page template
	HelpTPL = "pages/help"

	// FeaturesTPL page template
	FeaturesTPL = "pages/features"

	// FeatureRequestTPL page template
	FeatureRequestTPL = "pages/request"

	// SponsorshipTPL page template
	SponsorshipTPL = "pages/sponsorship"

	// SponsorsTPL page template
	SponsorsTPL = "pages/sponsors"
)

// About shows about page
func About(c *context.Context) {
	c.Data["Title"] = "About"

	c.HTML(200, AboutTPL)
}

// Faq shows faq page
func Faq(c *context.Context) {
	c.Data["Title"] = "FAQ"

	c.HTML(200, FaqTPL)
}

// Privacy shows privacy page
func Privacy(c *context.Context) {
	c.Data["Title"] = "Privacy Policy"

	c.HTML(200, PrivacyTPL)
}

// Tos shows tos page
func Tos(c *context.Context) {
	c.Data["Title"] = "Terms of Service"

	c.HTML(200, TosTPL)
}

// Brand shows brand page
func Brand(c *context.Context) {
	c.Data["Title"] = "Gitote Brand"

	c.HTML(200, BrandTPL)
}

// Contribute shows contribute page
func Contribute(c *context.Context) {
	c.Data["PageIsContribute"] = true
	c.Data["Title"] = "Contribute"

	c.HTML(200, ContributeTPL)
}

// Security shows security page
func Security(c *context.Context) {
	c.Data["Title"] = "Security"

	c.HTML(200, SecurityTPL)
}

// Verified shows verified page
func Verified(c *context.Context) {
	c.Data["Title"] = "Verified"

	c.HTML(200, VerifiedTPL)
}

// Makers shows maker page
func Makers(c *context.Context) {
	c.Data["Title"] = "Makers"

	c.HTML(200, MakersTPL)
}

// Help shows help page
func Help(c *context.Context) {
	c.Data["Title"] = "Help"
	c.Data["PageIsHelp"] = true

	c.HTML(200, HelpTPL)
}

// Contact shows contact page
func Contact(c *context.Context) {
	c.Data["Title"] = "Contact"

	c.HTML(200, ContactTPL)
}

// Features shows features page
func Features(c *context.Context) {
	c.Data["Title"] = "Features"
	c.Data["PageIsFeatures"] = true

	c.HTML(200, FeaturesTPL)
}

// FeatureRequest shows feature request page
func FeatureRequest(c *context.Context) {
	c.Data["Title"] = "Feature Request"

	c.HTML(200, FeatureRequestTPL)
}

// Sponsorship shows sponsorship page
func Sponsorship(c *context.Context) {
	c.Data["Title"] = "Sponsorship"

	c.HTML(200, SponsorshipTPL)
}

// Sponsors shows sponsorship page
func Sponsors(c *context.Context) {
	c.Data["Title"] = "Sponsorship"

	c.HTML(200, SponsorsTPL)
}
