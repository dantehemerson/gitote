// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package user

import (
	"gitote/gitote/pkg/context"
	"strings"
)

const (
	// EmbedTPL page template
	EmbedTPL = "embed/user"
)

// Embed returns user details for the embed
func Embed(c *context.Context) {
	ctxUser := GetUserByName(c, strings.TrimSuffix(c.Params(":username"), ""))
	c.Data["Owner"] = ctxUser

	c.HTML(200, EmbedTPL)
}
