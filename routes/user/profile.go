// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package user

import (
	"fmt"
	"gitote/gitote/models"
	"gitote/gitote/models/errors"
	"gitote/gitote/pkg/context"
	"gitote/gitote/pkg/setting"
	"gitote/gitote/pkg/tool"
	"gitote/gitote/routes/repo"
	"strings"

	"gitlab.com/yoginth/paginater"
)

const (
	// FollowersTPL page template
	FollowersTPL = "user/meta/followers"

	// StarsTPL page template
	StarsTPL = "user/meta/stars"
)

// GetUserByName get user name as parameter from URL
func GetUserByName(c *context.Context, name string) *models.User {
	user, err := models.GetUserByName(name)
	if err != nil {
		c.NotFoundOrServerError("GetUserByName", errors.IsUserNotExist, err)
		return nil
	}
	return user
}

// Profile shows profile page
func Profile(c *context.Context, puser *context.ParamsUser) {
	isShowKeys := false
	if strings.HasSuffix(c.Params(":username"), ".keys") {
		isShowKeys = true
	}

	// Show SSH keys.
	if isShowKeys {
		ShowSSHKeys(c, puser.ID)
		return
	}

	if puser.IsOrganization() {
		showOrgProfile(c)
		return
	}

	c.Title(puser.DisplayName())
	c.PageIs("UserProfile")
	c.Data["Owner"] = puser

	orgs, err := models.GetOrgsByUserID(puser.ID, c.IsLogged && (c.User.IsAdmin || c.User.ID == puser.ID))
	if err != nil {
		c.ServerError("GetOrgsByUserIDDesc", err)
		return
	}

	c.Data["Orgs"] = orgs

	tab := c.Query("tab")
	c.Data["TabName"] = tab
	switch tab {
	case "activity":
		retrieveFeeds(c, puser.User, -1, true)
		if c.Written() {
			return
		}
	default:
		page := c.QueryInt("page")
		if page <= 0 {
			page = 1
		}

		showPrivate := c.IsLogged && (puser.ID == c.User.ID || c.User.IsAdmin)
		c.Data["Repos"], err = models.GetUserRepositories(&models.UserRepoOptions{
			UserID:   puser.ID,
			Private:  showPrivate,
			Page:     page,
			PageSize: setting.UI.User.RepoPagingNum,
		})
		if err != nil {
			c.ServerError("GetRepositories", err)
			return
		}

		count := models.CountUserRepositories(puser.ID, showPrivate)
		c.Data["Page"] = paginater.New(int(count), setting.UI.User.RepoPagingNum, page, 5)
	}
	if puser.Suspended == true {
		c.Handle(404, "Suspended", err)
	} else {
		c.Success(ProfileTPL)
	}
}

// Followers shows followers page
func Followers(c *context.Context, puser *context.ParamsUser) {
	c.Title(puser.DisplayName())
	c.PageIs("Followers")
	c.Data["CardsTitle"] = c.Tr("user.followers")
	c.Data["Owner"] = puser
	repo.RenderUserCards(c, puser.NumFollowers, puser.GetFollowers, FollowersTPL)
}

// Following shows following page
func Following(c *context.Context, puser *context.ParamsUser) {
	c.Title(puser.DisplayName())
	c.PageIs("Following")
	c.Data["CardsTitle"] = c.Tr("user.following")
	c.Data["Owner"] = puser
	repo.RenderUserCards(c, puser.NumFollowing, puser.GetFollowing, FollowersTPL)
}

// Stars shows stars page
func Stars(c *context.Context) {

}

// Action shows user action page
func Action(c *context.Context, puser *context.ParamsUser) {
	var err error
	switch c.Params(":action") {
	case "follow":
		err = models.FollowUser(c.UserID(), puser.ID)
	case "unfollow":
		err = models.UnfollowUser(c.UserID(), puser.ID)
	}

	if err != nil {
		c.ServerError(fmt.Sprintf("Action (%s)", c.Params(":action")), err)
		return
	}

	redirectTo := c.Query("redirect_to")
	if !tool.IsSameSiteURLPath(redirectTo) {
		redirectTo = puser.HomeLink()
	}
	c.Redirect(redirectTo)
}
