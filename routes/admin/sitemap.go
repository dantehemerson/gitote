// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package admin

import (
	"gitote/gitote/models"
	"gitote/gitote/pkg/context"
	"gitote/gitote/routes"
)

const (
	// SitemapTPL page template
	SitemapTPL = "sitemap"

	// UserSitemapTPL page template
	UserSitemapTPL = "user/sitemap"

	// OrgSitemapTPL page template
	OrgSitemapTPL = "org/sitemap"

	// RepoSitemapTPL page template
	RepoSitemapTPL = "repo/sitemap"
)

// Sitemap shows about page
func Sitemap(c *context.Context) {
	c.HTML(200, SitemapTPL)
}

// UserSitemap shows about page
func UserSitemap(c *context.Context) {
	routes.RenderUserSearch(c, &routes.UserSearchOptions{
		Type:    models.UserTypeIndividual,
		Counter: models.CountUsers,
		Ranger:  models.Users,
		OrderBy: "id ASC",
		TplName: UserSitemapTPL,
	})
}

// OrgSitemap shows about page
func OrgSitemap(c *context.Context) {
	routes.RenderUserSearch(c, &routes.UserSearchOptions{
		Type:    models.UserTypeOrganization,
		Counter: models.CountOrganizations,
		Ranger:  models.Organizations,
		OrderBy: "id ASC",
		TplName: OrgSitemapTPL,
	})
}

// RepoSitemap shows about page
func RepoSitemap(c *context.Context) {
	page := c.QueryInt("page")

	var (
		repos []*models.Repository
		count int64
		err   error
	)

	repos, count, err = models.SearchRepositoryByName(&models.SearchRepoOptions{
		OrderBy: "id ASC",
		Private: false,
		Page:    page,
	})

	c.Data["Total"] = count

	if err = models.RepositoryList(repos).LoadAttributes(); err != nil {
		c.Handle(500, "LoadAttributes", err)
		return
	}
	c.Data["Repos"] = repos

	c.HTML(200, RepoSitemapTPL)
}
