// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package admin

import (
	"gitote/gitote/pkg/context"
)

const (
	// NewsTPL list page template
	NewsTPL = "admin/news/list"

	// NewsNewTPL page template
	NewsNewTPL = "admin/news/new"

	// NewsEditTPL page template
	NewsEditTPL = "admin/news/edit"
)

// News shows news page
func News(c *context.Context) {
	c.Data["Title"] = "News"
	c.Data["PageIsAdminNews"] = true
	c.HTML(200, NewsTPL)
}

// NewsNew shows new news page
func NewsNew(c *context.Context) {
	c.Data["Title"] = "News"
	c.Data["PageIsAdminNews"] = true
	c.HTML(200, NewsNewTPL)
}

// NewsEdit shows edit news page
func NewsEdit(c *context.Context) {
	c.Data["Title"] = "News"
	c.Data["PageIsAdminNews"] = true
	c.HTML(200, NewsEditTPL)
}
