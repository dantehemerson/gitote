// Copyright 2015 - Present, The Gogs Authors. All rights reserved.
// Copyright 2018 - Present, Gitote. All rights reserved.
//
// This source code is licensed under the MIT license found in the
// LICENSE file in the root directory of this source tree.

package dev

import (
	"gitote/gitote/models"
	"gitote/gitote/pkg/context"
	"gitote/gitote/pkg/setting"
)

//TemplatePreview pre render the template to preview it
func TemplatePreview(c *context.Context) {
	c.Data["User"] = models.User{Name: "Yoginth"}
	c.Data["AppVer"] = setting.AppVer
	c.Data["APIVer"] = setting.APIVer
	c.Data["AppURL"] = setting.AppURL
	c.Data["Code"] = "123456789"
	c.Data["ActiveCodeLives"] = setting.Service.ActiveCodeLives / 60
	c.Data["ResetPwdCodeLives"] = setting.Service.ResetPwdCodeLives / 60
	c.Data["CurDbValue"] = ""

	c.HTML(200, (c.Params("*")))
}
