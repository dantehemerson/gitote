{{template "base/head" .}}
<div class="admin config">
	<div class="ui container">
		<div class="ui grid">
			{{template "admin/navbar" .}}
			<div class="twelve wide column content">
				{{template "base/alert" .}}
				<h4 class="ui top attached header">
					Server Configuration 🖥
				</h4>
				<div class="ui attached table segment">
					<dl class="dl-horizontal admin-dl-horizontal">
						<dt>Application Name</dt>
						<dd>Gitote</dd>
						<dt>Application Version</dt>
						<dd>{{AppVer}}</dd>
						<dt>Application URL</dt>
						<dd>{{.AppURL}}</dd>
						<dt>Domain</dt>
						<dd>{{.Domain}}</dd>
						<dt>Offline Mode</dt>
						<dd><i class="fa fa{{if .OfflineMode}}-check{{end}}-square-o"></i></dd>
						<dt>Disable Router Log</dt>
						<dd><i class="fa fa{{if .DisableRouterLog}}-check{{end}}-square-o"></i></dd>

						<div class="ui divider"></div>

						<dt>Run User</dt>
						<dd>{{.RunUser}}</dd>
						<dt>Run Mode</dt>
						<dd>{{.RunMode}}</dd>

						<div class="ui divider"></div>

						<dt>Static File Root Path</dt>
						<dd><code>{{.StaticRootPath}}</code></dd>
						<dt>Log File Root Path</dt>
						<dd><code>{{.LogRootPath}}</code></dd>
						<dt>Reverse Authentication User</dt>
						<dd><code>{{.ReverseProxyAuthUser}}</code></dd>
					</dl>
				</div>

				<h4 class="ui top attached header">
					SSH Configuration 🔑
				</h4>
				<div class="ui attached table segment">
					<dl class="dl-horizontal admin-dl-horizontal">
						<dt>Enabled</dt>
						<dd><i class="fa fa{{if not .SSH.Disabled}}-check{{end}}-square-o"></i></dd>
						{{if not .SSH.Disabled}}
							<dt>Start Builtin Server</dt>
							<dd><i class="fa fa{{if .SSH.StartBuiltinServer}}-check{{end}}-square-o"></i></dd>
							<dt>Domain</dt>
							<dd>{{.SSH.Domain}}</dd>
							<dt>Port</dt>
							<dd>{{.SSH.Port}}</dd>
							<dt>Listen Port</dt>
							<dd>{{.SSH.ListenPort}}</dd>

							{{if not .SSH.StartBuiltinServer}}
								<dt>Root Path</dt>
								<dd>{{.SSH.RootPath}}</dd>
								<dt>Rewrite authorized_keys At Start</dt>
								<dd><i class="fa fa{{if .SSH.RewriteAuthorizedKeysAtStart}}-check{{end}}-square-o"></i></dd>
								<dt>Key Test Path</dt>
								<dd>{{.SSH.KeyTestPath}}</dd>
								<dt>Keygen ('ssh-keygen') Path</dt>
								<dd>{{.SSH.KeygenPath}}</dd>
								<dt>Minimum Key Size Check</dt>
								<dd><i class="fa fa{{if .SSH.MinimumKeySizeCheck}}-check{{end}}-square-o"></i></dd>
								{{if .SSH.MinimumKeySizeCheck}}
								<dt>Minimum Key Sizes</dt>
								<dd>{{.SSH.MinimumKeySizes}}</dd>
								{{end}}
							{{end}}
						{{end}}
					</dl>
				</div>

				<!-- Repository Configuration -->
				<h4 class="ui top attached header">
					Repository Configuration 📁
				</h4>
				<div class="ui attached table segment">
					<dl class="dl-horizontal admin-dl-horizontal">
						<dt>Repository Root Path</dt>
						<dd><code>{{.RepoRootPath}}</code></dd>
						<dt>Script Type</dt>
						<dd>{{.ScriptType}}</dd>
						<dt>Force Private</dt>
						<dd><i class="fa fa{{if .Repository.ForcePrivate}}-check{{end}}-square-o"></i></dd>
						<dt>Max Creation Limit</dt>
						<dd>{{.Repository.MaxCreationLimit}}</dd>
						<dt>Preferred Licenses</dt>
						<dd>{{Join .Repository.PreferredLicenses ", "}}</dd>
						<dt>Disable HTTP Git</dt>
						<dd><i class="fa fa{{if .Repository.DisableHTTPGit}}-check{{end}}-square-o"></i></dd>
						<dt>Enable Local Path Migration</dt>
						<dd><i class="fa fa{{if .Repository.EnableLocalPathMigration}}-check{{end}}-square-o"></i></dd>
						<dt>Commits Fetch Concurrency</dt>
						<dd>{{.Repository.CommitsFetchConcurrency}}</dd>
					</dl>
				</div>

				<!-- HTTP Configuration -->
				<h4 class="ui top attached header">
					HTTP Configuration 🌐
				</h4>
				<div class="ui attached table segment">
					<dl class="dl-horizontal admin-dl-horizontal">
						<dt>Access Control Allow Origin</dt>
						<dd>
							{{if .HTTP.AccessControlAllowOrigin}}
								<code>{{.HTTP.AccessControlAllowOrigin}}</code>
							{{else}}
								<i>(not set)</i>
							{{end}}
						</dd>
					</dl>
				</div>

				<!-- Database Configuration -->
				<h4 class="ui top attached header">
					Database Configuration 📙
				</h4>
				<div class="ui attached table segment">
					<dl class="dl-horizontal admin-dl-horizontal">
						<dt>Type</dt>
						<dd>{{.DbCfg.Type}}</dd>
						<dt>Host</dt>
						<dd>{{.DbCfg.Host}}</dd>
						<dt>Name</dt>
						<dd>{{.DbCfg.Name}}</dd>
						<dt>User</dt>
						<dd>{{.DbCfg.User}}</dd>
						<dt>SSL Mode</dt>
						<dd>{{.DbCfg.SSLMode}} (for "postgres" only)</dd>
						<dt>Path</dt>
						<dd>{{.DbCfg.Path}} (for "sqlite3" and "tidb")</dd>
					</dl>
				</div>

				<h4 class="ui top attached header">
					Service Configuration 🔧
				</h4>
				<div class="ui attached table segment">
					<dl class="dl-horizontal admin-dl-horizontal">
						<dt>Require Email Confirmation</dt>
						<dd><i class="fa fa{{if .Service.RegisterEmailConfirm}}-check{{end}}-square-o"></i></dd>
						<dt>Disable Registration</dt>
						<dd><i class="fa fa{{if .Service.DisableRegistration}}-check{{end}}-square-o"></i></dd>
						<dt>Show Register Button</dt>
						<dd><i class="fa fa{{if .Service.ShowRegistrationButton}}-check{{end}}-square-o"></i></dd>
						<dt>Require Sign In View</dt>
						<dd><i class="fa fa{{if .Service.RequireSignInView}}-check{{end}}-square-o"></i></dd>
						<dt>Mail Notification</dt>
						<dd><i class="fa fa{{if .Service.EnableNotifyMail}}-check{{end}}-square-o"></i></dd>
						<dt>Enable Captcha</dt>
						<dd><i class="fa fa{{if .Service.EnableCaptcha}}-check{{end}}-square-o"></i></dd>
						<div class="ui divider"></div>
						<dt>Active Code Lives</dt>
						<dd>{{.Service.ActiveCodeLives}} minutes</dd>
						<dt>Reset Password Code Lives</dt>
						<dd>{{.Service.ResetPwdCodeLives}} minutes</dd>
					</dl>
				</div>

				<h4 class="ui top attached header">
					Webhook Configuration 🎣
				</h4>
				<div class="ui attached table segment">
					<dl class="dl-horizontal admin-dl-horizontal">
						<dt>Queue Length</dt>
						<dd>{{.Webhook.QueueLength}}</dd>
						<dt>Deliver Timeout</dt>
						<dd>{{.Webhook.DeliverTimeout}} seconds</dd>
						<dt>Skip TLS Verify</dt>
						<dd><i class="fa fa{{if .Webhook.SkipTLSVerify}}-check{{end}}-square-o"></i></dd>
					</dl>
				</div>

				<h4 class="ui top attached header">
					Mailer Configuration 📬
				</h4>
				<div class="ui attached table segment">
					<dl class="dl-horizontal admin-dl-horizontal">
						<dt>Enabled</dt>
						<dd><i class="fa fa{{if .MailerEnabled}}-check{{end}}-square-o"></i></dd>
						{{if .MailerEnabled}}
							<dt>Subject Prefix</dt>
							<dd><code>{{.Mailer.SubjectPrefix}}</code></dd>
							<dt>Disable HELO</dt>
							<dd><i class="fa fa{{if .Mailer.DisableHelo}}-check{{end}}-square-o"></i></dd>
							<dt>Disable HELO</dt>
							<dd>{{.Mailer.Host}}</dd>
							<dt>User</dt>
							<dd>{{if .Mailer.User}}{{.Mailer.User}}{{else}}(empty){{end}}</dd><br>
							<form class="ui form" action="{{AppSubURL}}/admin/config/test_mail" method="post">
								{{.CSRFTokenHTML}}
								<div class="inline field ui left">
									<div class="ui input">
										<input type="email" name="email" required>
									</div>
								</div>
								<button class="ui green button" id="test-mail-btn">Send Test Email</button>
							</form>
						{{end}}
					</dl>
				</div>

				<h4 class="ui top attached header">
					Cache Configuration 🚄
				</h4>
				<div class="ui attached table segment">
					<dl class="dl-horizontal admin-dl-horizontal">
						<dt>Cache Adapter</dt>
						<dd>{{.CacheAdapter}}</dd>
						<dt>Cache Interval</dt>
						<dd>{{.CacheInterval}} seconds</dd>
						{{if .CacheConn}}
						<dt>Cache Connection</dt>
						<dd><code>{{.CacheConn}}</code></dd>
						{{end}}
					</dl>
				</div>

				<h4 class="ui top attached header">
					Session Configuration ⌛
				</h4>
				<div class="ui attached table segment">
					<dl class="dl-horizontal admin-dl-horizontal">
						<dt>Session Provider</dt>
						<dd>{{.SessionConfig.Provider}}</dd>
						<dt>Provider Config</dt>
						<dd><code>{{.SessionConfig.ProviderConfig}}</code></dd>
						<dt>Cookie Name</dt>
						<dd>{{.SessionConfig.CookieName}}</dd>
						<dt>GC Interval Time</dt>
						<dd>{{.SessionConfig.Gclifetime}} seconds</dd>
						<dt>Session Life Time</dt>
						<dd>{{.SessionConfig.Maxlifetime}} seconds</dd>
						<dt>HTTPS Only</dt>
						<dd><i class="fa fa{{if .SessionConfig.Secure}}-check{{end}}-square-o"></i></dd>
						<dt>Cookie Life Time</dt>
						<dd>{{.SessionConfig.CookieLifeTime}} seconds</dd>
					</dl>
				</div>

				<h4 class="ui top attached header">
					Picture Configuration 🖼
				</h4>
				<div class="ui attached table segment">
					<dl class="dl-horizontal admin-dl-horizontal">
						<dt>Disable Gravatar</dt>
						<dd><i class="fa fa{{if .DisableGravatar}}-check{{end}}-square-o"></i></dd>
						<div class="ui divider"></div>
						<dt>Enable Federated Avatars</dt>
						<dd><i class="fa fa{{if .EnableFederatedAvatar}}-check{{end}}-square-o"></i></dd>
					</dl>
				</div>

				<h4 class="ui top attached header">
					Git Configuration 🚧
				</h4>
				<div class="ui attached table segment">
					<dl class="dl-horizontal admin-dl-horizontal">
						<dt>Git Version</dt>
						<dd>{{.GitVersion}}</dd>
						<dt>Disable Diff Syntax Highlight</dt>
						<dd><i class="fa fa{{if .Git.DisableDiffHighlight}}-check{{end}}-square-o"></i></dd>
						<dt>Max Diff Lines (for a single file)</dt>
						<dd>{{.Git.MaxGitDiffLines}}</dd>
						<dt>Max Diff Characters (for a single line)</dt>
						<dd>{{.Git.MaxGitDiffLineCharacters}}</dd>
						<dt>Max Diff Files (to be shown)</dt>
						<dd>{{.Git.MaxGitDiffFiles}}</dd>
						<dt>GC Arguments</dt>
						<dd><code>{{.Git.GCArgs}}</code></dd>
						<div class="ui divider"></div>
						<dt>Migration Timeout</dt>
						<dd>{{.Git.Timeout.Migrate}} seconds</dd>
						<dt>Mirror Update Timeout</dt>
						<dd>{{.Git.Timeout.Mirror}} seconds</dd>
						<dt>Clone Operation Timeout</dt>
						<dd>{{.Git.Timeout.Clone}} seconds</dd>
						<dt>Pull Operation Timeout</dt>
						<dd>{{.Git.Timeout.Pull}} seconds</dd>
						<dt>GC Operation Timeout</dt>
						<dd>{{.Git.Timeout.GC}} seconds</dd>
					</dl>
				</div>

				<h4 class="ui top attached header">
					Log Configuration 📜
				</h4>
				<div class="ui attached log-config segment">
					<table class="ui very basic table">
						{{range .Loggers}}
							<tr>
								<td>Mode</td>
								<td>{{.Mode}}</td>
							<tr>
							</tr>
								<td>Options</td>
								<td><pre><code>{{.Config}}</code></pre></td>
							</tr>
						{{end}}
					</table>
				</div>
			</div>
		</div>
	</div>
</div>
{{template "base/footer" .}}
